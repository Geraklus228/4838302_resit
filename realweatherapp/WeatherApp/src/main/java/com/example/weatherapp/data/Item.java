package com.example.weatherapp.data;

import org.json.JSONObject;

/**
 * Created by agekiani on 22/06/15.
 */
public class Item implements JSONPOPULATOR {
    private Condition condition;

    public Condition getCondition() {
        return condition;
    }

    @Override
    public void poupulate(JSONObject data) {
        condition = new Condition();
        condition.poupulate(data.optJSONObject("condition"));

    }
}
