package com.example.weatherapp.data;

import org.json.JSONObject;

/**
 * Created by agekiani on 22/06/15.
 */
public class Units implements JSONPOPULATOR {
    private String temperature;

    public String getTemperature() {
        return temperature;
    }

    @Override
    public void poupulate(JSONObject data) {
        temperature = data.optString("temperature");

    }
}
