package com.example.weatherapp.service;

import com.example.weatherapp.data.Channel;

/**
 * Created by agekiani on 22/06/15.
 */
public interface WeatherServiceCallback {
    void serviceSuccess(Channel channel);
    void serviceFailure (Exception exception);
}
