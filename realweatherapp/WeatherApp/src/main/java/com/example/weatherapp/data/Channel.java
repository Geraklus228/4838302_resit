package com.example.weatherapp.data;

import org.json.JSONObject;

/**
 * Created by agekiani on 22/06/15.
 */
public class Channel implements JSONPOPULATOR {
    private Units units;
    private  Item item;

    public Units getUnits() {
        return units;
    }

    public Item getItem() {
        return item;
    }

    @Override
    public void poupulate(JSONObject data) {

        units = new Units();
        units.poupulate(data.optJSONObject("units"));

        item = new Item();
        item.poupulate(data.optJSONObject("item"));

    }
}
